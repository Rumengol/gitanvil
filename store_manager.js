const request = indexedDB.open("gitanvil","2");
let db;
request.onsuccess = function (event) {
    db = event.target.result;
    console.log(db)
}

// idb is per-domain : loading temporary modules will always reset the database
request.onupgradeneeded = function (event) {
    db = event.target.result;
    const articleStore = db.createObjectStore("articles", { keyPath: "id", autoIncrement: true });
    articleStore.createIndex("articleId", "articleId", { unique: false });
    articleStore.createIndex("date", "date");

    console.log("upgraded")
};


function createTransaction(mode) {
    const transaction = db.transaction(["articles"], mode);
    return transaction.objectStore("articles");
}

// Article data from WA API, in JSON format
export function saveArticle(articleData){
    
    var article = {
        articleId: articleData.data.id,
        name: articleData.name,
        date: new Date(),
        wordcount: articleData.data.wordcount,
        isManual: articleData.isManual,
        data: JSON.stringify(articleData.data)
    }

    if(!articleData.isManual){
        areAutoSavesFull(article.articleId, articleData.maxAutosaves)
        .then((res) => {
            if (res) deleteOldestAutoSave(article.articleId).then(console.log)
        })
    }

    return new Promise(function(resolve, reject) {
        const request = createTransaction("readwrite").add(article);

        request.onsuccess = (event) => {
            resolve({
                article:article,
                id: event.target.result
            })
        }
    })
}

export function getArticleById(articleId){
    console.log("trying to get article by id: " + articleId)

    return new Promise(function(resolve, reject) {
        const request = createTransaction("readonly").get(parseInt(articleId));

        request.onsuccess = (event) => {
            resolve(event.target.result)
        }

    })
}

export function getAllVersions(articleId){
    console.log("trying to get all versions")
    return new Promise(function(resolve, reject) {
        const articleStore = createTransaction("readonly");
        const index = articleStore.index("articleId");

        let allVersions = []

        index.openCursor().onsuccess = (event) => {
            const cursor = event.target.result;
            if(cursor){
                if(cursor.value.articleId == articleId){
                    console.log("found article: " + cursor.value.articleId)
                    allVersions.push({
                        article:cursor.value, 
                        id:cursor.primaryKey
                    })
                }
                cursor.continue();
            } else {
                resolve(allVersions)
            }
        }
    })
}

export function areAutoSavesFull(articleId, max){
    return new Promise(function(resolve, reject) {
        const articleStore = createTransaction("readonly");
        const index = articleStore.index("articleId");

        let autoSavesCount = 0

        index.openCursor().onsuccess = (event) => {
            const cursor = event.target.result;
            if(cursor){
                if(cursor.value.articleId == articleId && !cursor.value.isManual){
                    autoSavesCount++
                }
                if(autoSavesCount >= max){
                    resolve(true)
                }
                cursor.continue();
            } else {
                resolve(false)
            }
        }
    })
}

export function deleteOldestAutoSave(articleId){
    console.log("deleting oldest autosave")
    return new Promise(function(resolve, reject) {
        const articleStore = createTransaction("readwrite");
        const index = articleStore.index("date");
        
        index.openCursor().onsuccess = (event) => {
            const cursor = event.target.result;
            if(cursor){
                if(cursor.value.articleId == articleId && !cursor.value.isManual){
                    resolve(articleStore.delete(cursor.primaryKey))
                }
            } else {
                reject("The article doesn't have any autosaves. How did we get there?")
            }
        }

    })
}

export async function loadArticle(articleId, articleTime){
    var allArticles = await db.getAllFromIndex("articles","date",articleTime)
    for(const article of allArticles){
        if(article.articleId == articleId)
            return article.data;
    }
}