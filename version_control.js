import * as store from "/store_manager.js";
import { last, chainErr, getDate, DISALLOWED_FIELDS } from "/utils.js";
import { appkey } from "/env.js";

const WA_PREFIX = "https://www.worldanvil.com/api/external/boromir"
const WA_RE = /https:\/\/www.worldanvil.com\/world\/.+\/edit/
const VERSION = "0.1.0"
let tabid;
let maxAutosaves = 10
let disable_autosaves = false
let apikey = ""

browser.storage.onChanged.addListener((change) => {
    if(change.maxAutosaves){
        console.log("new max autosaves: " + change.maxAutosaves.newValue)
        maxAutosaves = change.maxAutosaves.newValue
    } else if (change.wa_apikey){
        console.log("apikey changed")
        apikey = change.wa_apikey.newValue
    }
})

browser.webNavigation.onCompleted.addListener((details) => {
    console.log("matched!")
    tabid = details.tabId
    init(details.tabId);
  }, { url: [{ urlMatches : "https://www.worldanvil.com/world/.*/edit" }] });

browser.tabs.onActivated.addListener(function(activeInfo) {
    // Get the URL of the tab the user just switched to.
    browser.tabs.get(activeInfo.tabId).then(function(tab) {
        if(WA_RE.test(tab.url) == true){
            console.log("matched with re")
            tabid = activeInfo.tabId
            init(activeInfo.tabId);
        }
    });
});

browser.runtime.onMessage.addListener((message) => {
    switch(message.type){
        case "save_version":
            console.log("got asked to save")
            getArticle(message)
                .then(store.saveArticle, chainErr)
                .then(updateList, chainErr)
                .then(console.log("Saved and updateds"))
                .catch(console.log)
            break;
        case "get_version":
            console.log("got asked to get a version")
            store.getArticleById(message.version)
            .then((article) => {sendVersion(message.isPreview, article)})
            break;
    }
})

function sendVersion(isPreview, article){
    if(isPreview){
        browser.tabs.sendMessage(tabid, {
            type: "show_preview",
            article: article
        })
    } else {
        let updated_fields = Object.fromEntries(
            Object.entries(JSON.parse(article.data))
            .filter(([key, value]) => !DISALLOWED_FIELDS.includes(key))
            .map(([key, value]) => [key, value != null && typeof(value) == "object" ? { id: value.id } : value])
        )
        console.log(JSON.parse(article.data))
        console.log(updated_fields)

        let xhr = new XMLHttpRequest()
        xhr.open("PATCH",WA_PREFIX + "/article?id=" + article.articleId)
        xhr = createHeader(xhr)
        xhr.onreadystatechange = function () {
            if(xhr.readyState === 4 && xhr.status === 200) {
                browser.tabs.sendMessage(tabid, {
                    type: "reload_page"
                })
            }
        };
        xhr.send(JSON.stringify(updated_fields))
    }
}

function createHeader(xhr){
    xhr.setRequestHeader("x-auth-token",apikey)
    xhr.setRequestHeader("x-application-key", appkey)
    xhr.setRequestHeader("Content-type","application/json")
    xhr.setRequestHeader("User-Agent",`GitAnvil (https://gitlab.com/Rumengol/gitanvil,${VERSION})`)
    return xhr
}

function getArticle(message){
    try{
        let tabUrl = message.url
        console.log("trying to get article")
        return new Promise(function(resolve, reject) {
            if(WA_RE.test(tabUrl)){
                console.log(tabUrl)
                let xhr = new XMLHttpRequest()
                let articleid = last(tabUrl.split("/edit")[0].split("/"))

                xhr.open("GET",WA_PREFIX + "/article?id=" + articleid + "&granularity=2")
                xhr = createHeader(xhr)
                xhr.onreadystatechange = function () {
                    if(xhr.readyState === 4 && xhr.status === 200) {
                        let articlejson = JSON.parse(xhr.response)
                        resolve({
                            data: articlejson,
                            name: message.name,
                            isManual: message.isManual,
                            maxAutosaves: maxAutosaves
                        })
                    }
                };
                xhr.send(null)
            }
        })
    }
    catch(error){
        console.log(error)
    }
}

function AddItemToList(article_idbdata){
    let article =article_idbdata.article,
    id = article_idbdata.id,
    saveMode = article.isManual ? "Saved Manually" : "Auto Save"
    return `<option value=${id}>${article.name || ""} ${getDate(article.date)} (${article.wordcount} words, ${saveMode})</option>\n`
}

function updateList(article_idbdata){
    let addition = AddItemToList(article_idbdata)
    console.log(addition)
    browser.tabs.sendMessage(tabid, {
        type: "update_list",
        addition: addition
    })
}

async function init(){
    console.log("in init")
    if (navigator.storage && navigator.storage.persist) {
        navigator.storage.persist().then(persistent => {
          if (persistent) {
            console.log("Storage will not be cleared except by explicit user action");
          } else {
            console.warn("Storage may be cleared by the UA under storage pressure.");
          }
        });
      }
    browser.storage.local.get("wa_apikey").then((key) => {
        apikey = key.wa_apikey
    })
    browser.storage.local.get("disable_autosave").then((key) => {
        disable_autosaves = key.disable_autosave
    })
    var list = ""
    browser.tabs.query({currentWindow: true, active: true}).then((tabs) => {
        let tab = tabs[0]; // Safe to assume there will only be one result
        let articleid = last(tab.url.split("/edit")[0].split("/"))
        
        console.log("article id: " + articleid)
        store.getAllVersions(articleid).then((articles) => {
            if(articles != []){
                articles.forEach((article) => {
                    list += AddItemToList(article)
                })
            } else {
                console.log("No article found for article id: " + articleid)
            }

            //console.log(list)
            console.log(tabid)

            browser.tabs.sendMessage(tabid, {
                type: "init",
                list: list
            }).then(console.log("Message sent !"), console.log("Error sending message !"))

        })
    })
    
}
