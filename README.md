# GitAnvil

A misleadingly-named version control Mozilla add on for WorldAnvil. (This is not quite like git, it is much simpler and even more prone to errors)

## Installation

1. Download the .xpi file of the latest release
2. Go to `about:addons`
3. Click on the gear, then `install a module from a file...`
4. Select the xpi file and install the extension

## How to use

This extension requires some of the broadest permissions, but is actually active on a single page (or rather type of page) : WorldAnvil's edit page. Once the extension is installed, go to edit any article, and you will see a new tab: `Version Control`

On this tab you will need to provide a [user API key](https://www.worldanvil.com/api/auth/key) first. Then, you're all set!


### Saves

There are two ways to trigger a save with GitAnvil. A save will download the content and metadata of the article as they currently are. They are persistent between browser sessions, but may be deleted when clearing the cache or cookies.

Also note that the saves are **local** and no syncing between device is available at the moment (might be in the future, but don't count on it).

You can trigger a **manual save** at any time by clicking the `Remember button`. You can also give a `Name` to these saves to remember their purpose.

**Automatic Saves** are triggered when quitting the editor, either by clicking a link on the page, closing the tab or the browser. They don't have a name, and there is a limit to how many auto saves can exist for a given article. By default it is **10**, but you can change this parameter. Once the limit is reached, the oldest one will be deleted when the next auto save is triggered.

### Rolling back to a specific version

When selecting a version in the `Load Save` dropdown, its information will be displayed in the `Preview` box, for you to evaluate. At this point, the save is *not* loaded. For it to be loaded, you have to actually click on the `Load` button. This will reload the page.

Note that while every property of the article are saved, some cannot be updated through the WA API, thus rolled back automatically. This include **timelines**,**secrets**,**historical events**,**organisation chart** and some more. See the ``DISALLOWED_FIELD`` constant in [utils.js](/utils.js) for a complete list of what cannot be updated. Again, all these informations are still saved and visible.

**Warning**: Any change not saved will be overwritten by a load action! It is recommended that you save your article in GitAnvil before loading a previous save.

## License

This code is licensed by the GNU Affero General Public License, which basically means that you can do whatever you want with this code, as long as it's open source. See [LICENSE](/LICENSE) for more information.

## Contributing

Suggestions and contributions are welcomed, especially regarding the appearance. I try to stick to WA design as much as possible, but improvements are possible. To develop the add on on your own, copy this repository and create an `env.js` file in  the root directory, containing this line:

```js
export const appkey="[Your WA application token]"
```