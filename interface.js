let initialized = false

browser.runtime.onMessage.addListener((message) => {
    switch(message.type){
        case "init":
            init(message.list)
            break;
        case "show_preview":
            console.log(message)
            updatePreview(message.article)
            break;
        case "update_list":
            console.log(message)
            updateList(message.addition)
            break;
        case "load_article":
            console.log(message)
            loadArticle(message)
            break;
        case "reload_page":
            window.location.reload()
            break;
    }
})



function autosave() {
    if(!initialized) return
    window.onbeforeunload -= autosave
    let disable_autosaves = document.getElementsByName("disable_autosave")[0].checked
    let maxAutosaves = document.getElementsByName("maxAutosaves")[0].value
    
    if(!disable_autosaves){
        browser.runtime.sendMessage({
            type: "save_version",
            url: window.location.href,
            name: "",
            isManual: false,
            maxAutosaves: maxAutosaves
        })
        setTimeout(() => window.onbeforeunload += autosave, 100)        
    }
}

async function init(list){
    window.onbeforeunload += autosave
    // await new Promise(resolve => setTimeout(resolve, 10000))
    maxSaves = await browser.storage.local.get("maxAutosaves")
    apiKey = await browser.storage.local.get("wa_apikey")
    no_autosave = await browser.storage.local.get("disable_autosave")

    const navitem = `
    <li class="nav-item">
        <a class="nav-link gitanvil" href="#gitanvil" role="tab" data-toggle="tab" aria-selected="false">Version Control</a>
    </li>
    `

    const maintab = `
    <div id="gitanvil" class="tab-pane" role="tabpanel">
        <div class="card-box">
            <label for="apikey">User Api Key</label>
            <input class="form-control" type="text" name="apikey", value="${apiKey.wa_apikey ||""}">
            <br>
            <div class="row">
                <div class="col-md-10">
                    <label for="commitname">Name</label>
                    <input class="form-control" type="text" name="commitname">
                </div>
                <div class="col-md-1">
                    <label for="maxAutosaves">Max Autosaves</label>
                    <input class="form-control" type="number" name="maxAutosaves" value=${parseInt(maxSaves.maxAutosaves) || 10}>
                </div>
                <div class="col-md-1">
                    <label for="disable_autosave">Disable Autosaves</label>
                    <input class="form-control" type="checkbox" name="disable_autosave" ${no_autosave.disable_autosave ? "checked" : ""}>
                </div>
            </div>
            <br>
            
            <select class="select2 form-control">
                <option value="example">Load Save...</option>
                ${list}
            </select>
            <div class="row mb-4 mt-4">
                <div class="col ml-1">
                    <a class="btn btn-primary btn-block d-block rem-version-control">Remember</a>
                </div>
                <div class="col ml-1 mr-1">
                    <a class="btn btn-primary btn-block d-block load-version">Load</a>
                </div>
            </div>
            
            <p>Preview:</p>
            <texarea class="txt-test form-control" rows="15" cols="150"></textarea>
        </div>
    </div>
    `
    if(document.querySelector("#gitanvil") == undefined){
        try{
            document.querySelector("#article-edit-container .article-navigation>.context-menu>.nav").innerHTML += navitem
            document.querySelector("#article-edit-container>div>.tab-content").innerHTML += maintab

            //document.querySelector(".gitanvil").addEventListener("click",openTab)
            document.querySelector(".rem-version-control").addEventListener("click",saveVersion)
            document.querySelector(".load-version").addEventListener("click",loadVersion)
            document.querySelector("#gitanvil .select2").addEventListener("change",loadPreview)
            document.getElementsByName("apikey")[0].addEventListener("change", (key) =>{
                console.log("Changed Api Key")
                browser.storage.local.set({
                    wa_apikey: key.target.value
                })
            })
            document.getElementsByName("maxAutosaves")[0].addEventListener("change", (n) =>{
                console.log("Changed max autosaves")
                browser.storage.local.set({
                    maxAutosaves: n.target.value
                })
            })
            document.getElementsByName("disable_autosave")[0].addEventListener("change", (n) =>{
                console.log("Changed disable autosaves")
                browser.storage.local.set({
                    disable_autosave: n.target.checked
                })
            })
            console.log("successfully loaded")
            initialized = true
        }
        catch {(e) => {console.log(e)}}
    }
}

function openTab(){
    console.log("Opened!")
    try{
        let test = document.querySelector("#gitanvil div.card-box")
        browser.runtime.sendMessage({
            type: "fetch_article",
        }).then(console.log("fetch asked"), (e) => test.innerHTML += "Cannot fetch article at opening. Reason: " + e)
    }
    catch(e){
        console.log(e)
    }
}
function displayItem(entry, depth = 1){
    return Object.entries(entry).map(([key, value]) => {
        console.log(value)
        if (value && typeof(value) == "object"){
            if(Object.keys(value).length === 0)
                value = "[]"
            else
                value = "[\n" + displayItem(value, depth+1) + "\n" + "\t".repeat(depth) + "]"
        }
        return `${"\t".repeat(depth)}${key} = ${value}`
    }).join("\n")
}
function updatePreview(article){

    console.log("Updating Preview")
    try {
        console.log(article)
        let textarea = document.querySelector(".txt-test")    
        
        textarea.innerHTML = Object.entries(JSON.parse(article.data))
            .map(([key, value]) => {
                if (value && typeof(value) == "object"){
                    if(Object.keys(value).length === 0)
                        value = "[]"
                    else
                        value = "[\n" + displayItem(value) + "\n" + "]"
                }
                return `${key} = ${value}`
        }).join("\n")
    }
    catch(e){
        console.log(e)
    }
}

function updateList(line){
    console.log("Updating List")
    if(document.querySelector("#gitanvil") != undefined){
        let selection = document.querySelector("#gitanvil .select2")
        selection.innerHTML += line
    }
}

function saveVersion(){
    console.log("In save!")
    try{
        let name = document.getElementsByName("commitname")[0].value
        let textarea = document.querySelector(".txt-test")
        let maxAutosaves = document.getElementsByName("maxAutosaves")[0].value
        browser.runtime.sendMessage({
            type: "save_version",
            url: window.location.href,
            isManual: true,
            name: name,
            maxAutosaves: maxAutosaves
        }).then(console.log("Save asked"), (e) => textarea.innerHTML += "Cannot fetch article at save. Reason: " + e)
    }
    catch(e){
        console.log(e)
    }
}

function askVersion(isPreview){
    version = document.querySelector("#gitanvil .select2").value
    if(version == "example"){
        console.log("no version selected")
        return
    }
    console.log("asking for version " + version)

    browser.runtime.sendMessage({
        type: "get_version",
        version: version,
        isPreview: isPreview
    })
}

function loadPreview(){
    askVersion(true)
}

function loadVersion(){
    askVersion(false)
}