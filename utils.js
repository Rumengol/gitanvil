export const DISALLOWED_FIELDS = ["templateType",
                                "id",
                                "url",
                                "folderId",
                                "updateDate",
                                "subscribergroups",
                                "isEditable",
                                "succes",
                                "creationDate",
                                "publicationDate",
                                "notificationDate",
                                "likes",
                                "views",
                                "userMetadata",
                                "articleMetadata",
                                "slug",
                                "success",
                                "wordcount",
                                "datasheet",
                                "entityClass",
                                "customArticleTemplate",
                                "timeline",
                                "prompt",
                                "secrets",
                                "histories",
                                "editURL",
                                "orgchart"]
export const MAYBEVALID_PATCH_FIELDS = ["content"]

export function last(array) {
    return array[array.length - 1];
}

export function chainErr(err){
    return Promise.reject(err)
}

export function getDate(date){
    let cDate = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate();
    let cTime = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    let dateTime = cDate + ' ' + cTime;
    return dateTime;
}